'use strict';

var expect = require('chai').expect;
var assert = require('chai').assert;
var should = require('chai').should();


const Calculator = require('../src');
let myCalculator = new Calculator();

describe('All tests', () => {
	describe('Test for function add(numbers)..', () => {

		it('0 by default', ()=> {
			myCalculator.add("").should.equal(0);
		});

		it('",1,2,0" -> 3', ()=> {
			myCalculator.add(",1,2,0 - 3").should.equal(3);
		});

		it('",,,,,," -> 0', ()=> {
			myCalculator.add(",,,,,,").should.equal(0);
		});

		it('"1,1,1,1,1,1,1" -> 7', ()=> {
			myCalculator.add("1,1,1,1,1,1,1").should.equal(7);
		});

		it('"1\n2,3" -> 6', ()=> {
			myCalculator.add("1\n2,3").should.equal(6);
		});

		it('"1,\n" -> Error (,\\n) or (\\n,)', ()=> {
			myCalculator.add("1,\n").should.equal('Error (,\\n) or (\\n,)');
		});




		it('"//<;>\n1;2" -> 3', ()=> {
			myCalculator.add("//<;>\n1;2").should.equal(3);
		});
		it('"//<*>\n1*2*3" -> 6', ()=> {
			myCalculator.add("//<*>\n1*2*3").should.equal(6);
		});



		it('"-1, 9" -> Отрицательные числа не допустимы: [-1]', ()=> {
			myCalculator.add("-1, 9").should.equal('Отрицательные числа не допустимы: [-1]');
		});

		it('"-1, -23, 19, -74" -> Отрицательные числа не допустимы: [-1, -23, 19, -74]', ()=> {
			myCalculator.add("-1, -23, 19, -74").should.equal('Отрицательные числа не допустимы: [-1,-23,-74]');
		});

		it('"2, 1001" -> 2', ()=> {
			myCalculator.add("2, 1001").should.equal(2);
		});



		it('"//<***>\n1***2***3" -> 6', ()=> {
			myCalculator.add("//<***>\n1***2***3").should.equal(6);
		});



		it('"//<*><%>\n1*2%3" -> 6', ()=> {
			myCalculator.add("//<*><%>\n1*2%3").should.equal(6);
		});

		it('"//<*>\n1*2,3" -> 6', ()=> {
			myCalculator.add("//<*><%>\n1*2%3").should.equal(6);
		});

		it.skip('"/<*>\n1*2,3" -> Error', ()=> {
			myCalculator.add("/<*><%>\n1*2%3").should.equal(6);
		});

// !!
		it('"//<*>\n1*2,9" -> 12', ()=> {
			myCalculator.add("//<*>\n1*2,9").should.equal(12);
		});

		it('"//<**:&>\n1**:&2,9" -> 12', ()=> {
			myCalculator.add("//<**:&>\n1**:&2,9").should.equal(12);
		});

		it('"//<sdf><|%#><,,><*******>< >\n28<*******>32\n< >19<sdf>11<|%#>10" -> 100', ()=> {
			myCalculator.add("//<sdf><|%#><,,><*******>< >\n28<*******>32\n< >19<sdf>11<|%#>10").should.equal(100);
		});

	});

});