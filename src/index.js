class Calculator {
	constructor() {
		console.log('Calculator init!')
	}



	add(numbers){
		var allSeparator = /[<,>?<\n>]/;

		if(/\/\/<.*>\n/.test(numbers)){
			
			let cpecSeparators =  numbers
					.match(/<.*>/)[0]
					.replace("><", ">?<");

			allSeparator = new RegExp('[<,>?' + cpecSeparators + '<\n>]');
			numbers = numbers.slice((2));
		};



		function refactorResult(data){
			let result;
			let sum = 0;
			let StatusSum = true;
			let ErrorArray = [];

			if(/(,\n)|(\n,)/.test(data)){
				result = "Error (,\\n) or (\\n,)" ;
			} else {
				result = data
				.split(allSeparator)
				.map(x => { 
					return x === "" ? 0 : parseInt(x)
				})
				.filter(x => {
					if(x < 0){
						ErrorArray.push(x);
						StatusSum = false;
					};
					return (x <= 1000) && (x >= 0);
				})
				.reduce((x, s) => {
					return x + s;
				}, 0);
			}
			if(!StatusSum){
				return 'Отрицательные числа не допустимы: ['+ ErrorArray +']';
			} else {
				return result;
			}
		};


		return refactorResult(numbers);
	}
}
module.exports = Calculator;